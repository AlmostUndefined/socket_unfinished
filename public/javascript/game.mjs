import { createElement, addClass, removeClass } from "./helper.mjs";
const createRoomBtn = document.getElementById("create-room");
const username = sessionStorage.getItem("username");
const roomContainer = document.getElementById("room-container");
const gameBtn = document.getElementById("game-button");
const backToRoomsBtn = document.getElementById("back-to-rooms");

if (!username) {
  window.location.replace("/login");
}

createRoomBtn.addEventListener("click", createRoom);

function createRoom() {
  let room = prompt('Input Room Name', '');
  if (room) {
    console.log(room)
    socket.emit("createRoom", room);
  } else {
    alert('Name for this room is invalid')
  }
};

const socket = io("", { query: { username } });

const createRooms = roomId => {
  const room = createElement({
    tagName: "div",
    className: "room flex-centered no-select",
    attributes: { id: roomId }
  });

  const roomName = createElement({
    tagName: "h2",
    className: "room-name"
  });

  const roomButton = createElement({
    tagName: "button",
    className: "roomBtn"
  });

  roomName.innerText = roomId;
  roomButton.innerText = "Join";

  room.append(roomName);
  room.append(roomButton);

  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", roomId);
  };

  roomButton.addEventListener("click", onJoinRoom);

  const leaveRoom = () => {
    const roomsPage = document.getElementById("rooms-page");
    removeClass(roomsPage, "display-none");
  
    const gamePage = document.getElementById("game-page");
    addClass(gamePage, "display-none")
    
    socket.emit("LEAVE_ROOM", {username, roomId});
  }
  
  backToRoomsBtn.addEventListener("click", leaveRoom);

  return room;
};

const updateRooms = rooms => {
  console.log(1)
  const allRooms = rooms.map(createRooms);
  console.log(allRooms)
  roomContainer.append(...allRooms);
};

const joinRoomDone = ({ username, roomId }) => {  
  const playersContainer = document.getElementById("players-container");
  const gameHeader = document.getElementById("game-header");
  gameHeader.innerText = roomId;

  const roomsPage = document.getElementById("rooms-page");
  addClass(roomsPage, "display-none");

  const gamePage = document.getElementById("game-page");
  removeClass(gamePage, "display-none")

  const player = createElement({
    tagName: "div",
    className: "player-container",
    attributes: { id: username }
  });

  const playerHeader = createElement({
    tagName: "h2"
  });

  const playerIndicator = createElement({
    tagName: "div",
    className: "indicator"
  });

  playerHeader.innerText = username;
  player.append(playerHeader, playerIndicator);
  playersContainer.append(player)
};

function readyToggle() {
  if (gameBtn.innerText == 'READY') {
    gameBtn.innerText = 'NOT READY';
  } else {
    gameBtn.innerText = 'READY';
  }
}

gameBtn.addEventListener("click", readyToggle);

const leaveRoomDone = ({username, roomId}) => {
  console.log(username)
  const elem = document.getElementById(username);
  elem.remove();
}


socket.on('invalidName', () => {
  alert(`This name is already used`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
})

socket.on('sendRoomsList', updateRooms);
socket.on("JOIN_ROOM_DONE", joinRoomDone);
socket.on("LEAVE_ROOM_DONE", leaveRoomDone);