import * as config from "./config";
import { createSocket } from "dgram";

const clients = [];
const rooms = [];
const counterMap = new Map(rooms.map(roomId => [roomId, 0]));

const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomId => counterMap.has(roomId));

export default io => {
  io.on("connection", socket => {
    socket.emit("sendRoomsList", rooms);
    const username = socket.handshake.query.username;
    if (!(clients.indexOf(username) && username)) {
      socket.emit("invalidName");
    }
    clients.push(username);

    socket.on("createRoom", room => {
      rooms.push(room);
      console.log(rooms)
      socket.emit("sendRoomsList", rooms);
      socket.broadcast.emit("sendRoomsList", rooms);
    })

    socket.on("JOIN_ROOM", roomId => {
      socket.join(roomId, () => {
        io.emit("JOIN_ROOM_DONE", { username, roomId });
      });
    });

    socket.on("LEAVE_ROOM", ({username, roomId}) => {
      socket.leave(roomId, () => {
        io.emit("LEAVE_ROOM_DONE", { username, roomId });
      });
    })

    socket.on("disconnect", () => {
      const index = clients.lastIndexOf(username);
      clients.splice(index, 1)
      console.log(clients, 'disc');
    })
  });
};
